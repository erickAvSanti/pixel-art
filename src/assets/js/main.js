
let jQuery = require('jquery');
let THREE = require('three');
let width = 400;
let height = 400;

let project_scale_percent = 100;
let project_scale = 1;
let project_step_def = 8;
let project_step = 8;
let project_step_last = 8;
let project_step_scaled = 8;

let canvas,ctx,app=null;

let line_draw_start = null;
let line_draw_end 	= null;
let line_draw_points = [];
let cursor_draw 	= false;
let cursor_x = 0;
let cursor_y = 0; 
let transparent_enabled = false; 
let request_anim_id = null;
function getAppValues(app){ 
	width = app.project_width;
	height = app.project_height;
	transparent_enabled = app.transparent.enabled == 'ENABLED';

	getScalePercent();
	canvas.width = width*project_scale_percent;
	canvas.height = height*project_scale_percent ;
	project_scale = app.project_scale;
	project_step_last = project_step_scaled;
	project_step_scaled = project_step*project_scale_percent; 
	if(project_step!=project_step_last){
		let point,pointX,pointY,step,modX,modY;
		for(let i in layers){
			let points = layers[i]; 
			for(let j in points){
				point = points[j]; 
				pointX = point.x;
				pointY = point.y;  
				step = point.step;  
				
				modX = pointX / step;
				modY = pointY / step;

				delete points[j];

				point.x 	= modX * project_step_scaled;
				point.y 	= modY * project_step_scaled;
				point.step = project_step_scaled;
		    	point.x = window.parseFloat(point.x.toFixed(1));
		    	point.y = window.parseFloat(point.y.toFixed(1));
				points[point.x+"-"+point.y] = point;
			}
		} 
		if(line_draw_start && line_draw_end){
			point 	= line_draw_start; 
			pointX 	= point.x;
			pointY 	= point.y;  
			step 	= point.step;  
			
			modX 	= pointX / step;
			modY 	= pointY / step;

			point.x 	= modX * project_step_scaled;
			point.y 	= modY * project_step_scaled;
			point.step = project_step_scaled;
			/*-----------------------------------------*/
			point 	= line_draw_end; 
			pointX 	= point.x;
			pointY 	= point.y;  
			step 	= point.step;  
			
			modX 	= pointX / step;
			modY 	= pointY / step;

			point.x 	= modX * project_step_scaled;
			point.y 	= modY * project_step_scaled;
			point.step = project_step_scaled;

		}
	}
	setDrawerMode();
}
function getScalePercent(){
	let scale = 100;
	switch(project_scale){
		case 1:
			scale = 100;
		break; 
		case 2:
			scale = 120;
		break; 
		case 3:
			scale = 150;
		break; 
		case 4:
			scale = 180;
		break; 
		case 5:
			scale = 210;
		break; 
		case 6:
			scale = 240;
		break; 
		case 7:
			scale = 270;
		break; 
		case 8:
			scale = 300;
		break;
		case 9:
			scale = 340;
		break; 
		case 10:
			scale = 370;
		break; 
		default:
			scale = 100;
		break;
	}
	project_scale_percent = scale==100 ? 1 : scale/100;
}
function func(_app){ 
	app = _app;
	canvas = document.getElementById('drawer');
	ctx = canvas.getContext('2d');
	getAppValues(_app); 
	init();

}
function setDrawerMode(){
	initDrawerModePoint();
	initDrawerModeLine();
}
function initDrawerModePoint(){
	let drawer_mode = document.getElementById('drawer_mode_point');
	drawer_mode.width=25;
	drawer_mode.height=25;
	let ctx = drawer_mode.getContext('2d');
	if(app.drawer_mode_selected==1){
		ctx.save();
		ctx.beginPath();
		ctx.strokeStyle='red';
		ctx.lineWidth=5;
		ctx.strokeRect(0,0,drawer_mode.width,drawer_mode.height);
		ctx.restore();
	}
	ctx.beginPath();
	ctx.fillStyle='black';
	ctx.fillRect(2,2,21,21);
}
function initDrawerModeLine(){
	let drawer_mode = document.getElementById('drawer_mode_line');
	drawer_mode.width=25;
	drawer_mode.height=25;
	let ctx = drawer_mode.getContext('2d');
	if(app.drawer_mode_selected==2){
		ctx.save();
		ctx.beginPath();
		ctx.strokeStyle='red';
		ctx.lineWidth=5;
		ctx.strokeRect(0,0,drawer_mode.width,drawer_mode.height);
		ctx.restore();
	}
	ctx.beginPath();
	ctx.fillStyle='black';
	ctx.lineWidth=5;
	ctx.moveTo(3,3);
	ctx.lineTo(22,22);
	ctx.stroke();
}

function init(){
	render();
}
function render(){
	if(ctx){
		if(app && app.project_redraw){  
			getAppValues(app);
			anim();
			app.project_redraw = false;
		}
	}
	request_anim_id = requestAnimationFrame(render);

}
function drawGrid(){ 
	ctx.save();
	if(!transparent_enabled){
		ctx.beginPath();
		ctx.lineWidth=0; 
		ctx.fillStyle='#FFFFFF';
		ctx.fillRect(0,0,width*project_scale_percent,height*project_scale_percent) ;
	}
	var flag = false;
	var first_flag = flag;
	for(var i=0;i<width*project_scale_percent;i+=project_step_scaled){  
		first_flag = flag;
		for(var j=0;j<height*project_scale_percent;j+=project_step_scaled){
			ctx.beginPath();
			ctx.lineWidth=1;
			ctx.strokeStyle='#88888833';
			if (flag) {
				ctx.fillStyle='#777777';
			}else{ 
				ctx.fillStyle='#EEEEEE';
			} 
			ctx.rect(i,j,project_step_scaled,project_step_scaled) ;
			ctx.stroke();
			flag = !flag ;
		}
		if(first_flag==flag){
			flag = !flag;
		}
	}
	ctx.restore();
}
function drawCursor(){
	if(cursor_draw){ 

		ctx.save();
		ctx.beginPath() ;
		ctx.lineWidth=2;
		ctx.strokeStyle='#FF0000' ; 
		ctx.strokeRect(cursor_x,cursor_y,project_step_scaled,project_step_scaled);
		ctx.restore();
	}
}
function drawCircle(){

	ctx.beginPath();
	ctx.lineWidth=30;
	ctx.fillStyle='#000000';
	ctx.arc(width/2,height/2,width/2,0,Math.PI*2);
	ctx.fill();
}
function anim(){ 
	ctx.clearRect(0,0,width,height);
	drawGrid();
	drawInCanvas() ;
	drawCursor();
}
function drawInCanvas(){ 
	ctx.save(); 
	let arr = app.project_layers.slice(0,app.project_layers.length).reverse();
	let i,j,point,tmp,_data;
	for(i in arr){
		let p_layer = arr[i];
		if(p_layer.id in layers && p_layer.visible){

			let points = layers[p_layer.id]; 
			for(j in points){
				let point = points[j];
				ctx.beginPath();
				ctx.lineWidth=0;
				ctx.fillStyle=point.color;
				let pointX = point.x;
				let pointY = point.y; 
				ctx.fillRect(pointX,pointY,point.step,point.step);
			}
		}
	}
	if(app.drawer_mode_selected==2 && line_draw_start && line_draw_end){
		let p0 = new THREE.Vector2(
				line_draw_start.x+line_draw_start.step/2,
				line_draw_start.y+line_draw_start.step/2
			);
		let pf = new THREE.Vector2(
				line_draw_end.x+line_draw_end.step/2,
				line_draw_end.y+line_draw_end.step/2
			);
		let v = pf.clone().sub(p0);
		let vUnit = v.clone().normalize();
		let length = v.length(); 
		line_draw_points.splice(0,line_draw_points.length);
		for(i=0; i<=length;i+=line_draw_start.step){
			 tmp = p0.clone().addScaledVector(vUnit,i);
			 _data = {
	    		x:(tmp.x - tmp.x%project_step_scaled),
	    		y:(tmp.y - tmp.y%project_step_scaled),
				color:line_draw_start.color,  
				step:project_step_scaled,
	    	};
	    	_data.x = window.parseFloat(_data.x.toFixed(1));
	    	_data.y = window.parseFloat(_data.y.toFixed(1));
	    	line_draw_points.push(_data);
		}
		for(i=0;i<line_draw_points.length;i++){ 
			point = line_draw_points[i];
			ctx.beginPath();
			ctx.lineWidth=0;
			ctx.fillStyle=point.color; 
			ctx.fillRect(point.x,point.y,point.step,point.step);
		}
 
		ctx.beginPath();
		ctx.lineWidth=0;
		ctx.fillStyle=line_draw_start.color; 
		ctx.fillRect(line_draw_start.x,line_draw_start.y,line_draw_start.step,line_draw_start.step);
		ctx.beginPath();
		ctx.lineWidth=0;
		ctx.fillStyle=line_draw_end.color; 
		ctx.fillRect(line_draw_end.x,line_draw_end.y,line_draw_end.step,line_draw_end.step);  
	} 
	ctx.restore();
}

let drawer = null;
let drawer_draw = false;
let drawer_cursor_x = 0;
let drawer_cursor_y = 0;
let app_container = null;
let jQuerydragging = null;
let dragging_offsetX = 0;
let dragging_offsetY = 0;

let drawer_points = [];
let points_to_draw = [];

let layers = {};
window._layers = layers;

jQuery('window').ready(function(evt){ 
	let zindex = 10;
	jQuery('.pop-over').each(function(idx,obj){
		jQuery(obj).css('z-index',zindex)
		zindex+=10;
	});
	jQuery(window).blur(function(evt){
		console.log('blur',evt);
		jQuerydragging = null;
	});
	jQuery(window).focus(function(evt){
		console.log('focus',evt);
	});

 	app_container = jQuery('.app-container');
 	drawer = jQuery('#drawer');

    app_container.on("mousemove", function(e) {
        if (jQuerydragging) {
        	let _top = e.pageY - dragging_offsetY;
        	let _left = e.pageX - dragging_offsetX;
        	if(_top<0)_top=0;
        	if(_left<0)_left=0;
            jQuerydragging.offset({
                top: _top,
                left: _left
            });
        }
    });
 
    app_container.on("mousedown", "div.pop-hover-bar", function (e) { 
    	dragging_offsetX = e.offsetX;
    	dragging_offsetY = e.offsetY;
        jQuerydragging = jQuery(e.target).parent();
        if(jQuerydragging.hasClass('pop-hover-bar')){
        	jQuerydragging = jQuerydragging.parent();  
        } else{  
        }
    });

   	app_container.on("mouseup", function (e) {
        jQuerydragging = null;
    });

    drawer.mousemove(function(e) {
    	if(!app)return; 
    	cursor_draw = true;
    	cursor_x = e.offsetX - e.offsetX%project_step_scaled;
    	cursor_y = e.offsetY - e.offsetY%project_step_scaled; 

    	if(drawer_draw){
	    	let _data = {
	    		x:(e.offsetX - e.offsetX%project_step_scaled),
	    		y:(e.offsetY - e.offsetY%project_step_scaled),
				color:app.drawer_color.hex8,  
				step:project_step_scaled,
	    	}; 
	    	_data.x = window.parseFloat(_data.x.toFixed(1));
	    	_data.y = window.parseFloat(_data.y.toFixed(1));
	    	if(app.drawer_mode_selected==1){
	    		points_to_draw[_data.x+"-"+_data.y] = _data; 
	    	} 
	    	if(app.drawer_mode_selected==2){
	    		line_draw_end = _data; 
	    	} 
	    	if(app.drawer_mode_selected==3){
	    		if(_data.x+"-"+_data.y in points_to_draw){
	    			delete points_to_draw[_data.x+"-"+_data.y];
	    		}
	    	} 
    	}
    	app.project_redraw = true;
    });

    drawer.mousedown(function(e) {
    	console.log(e);
    	if(e.button==0){
    		drawer_draw = true; 
	    	if(!(app.selected_layer.id in layers)){
	    		layers[app.selected_layer.id] = {}; 
	    	}
	    	points_to_draw = layers[app.selected_layer.id];  
	    	let _data = {
	    		x:(e.offsetX - e.offsetX%project_step_scaled),
	    		y:(e.offsetY - e.offsetY%project_step_scaled),
				color:app.drawer_color.hex8,  
				step:project_step_scaled,
	    	};
	    	_data.x = window.parseFloat(_data.x.toFixed(1));
	    	_data.y = window.parseFloat(_data.y.toFixed(1));
	    	if(app.drawer_mode_selected==1){
	    		points_to_draw[_data.x+"-"+_data.y] = _data; 
	    	} 
	    	if(app.drawer_mode_selected==2){
	    		line_draw_start = _data; 
	    	} 
    		app.project_redraw = true;
    	}
    	app.drawer_enable_scaling = false;
    });
    drawer.mouseup(function(e) {
    	let i,j,point,_data;
    	if(e.button==0){
    		drawer_draw = false ;
    		if(app.drawer_mode_selected==2){ 
    			if(!(app.selected_layer.id in layers)){
		    		layers[app.selected_layer.id] = {}; 
		    	}
		    	points_to_draw = layers[app.selected_layer.id]; 
				for(i=0;i<line_draw_points.length;i++){ 
					_data = line_draw_points[i]; 
	    			points_to_draw[_data.x+"-"+_data.y] = _data; 
				}
				line_draw_start = null;
				line_draw_end = null;
    		}
    	}
    	app.drawer_enable_scaling = true;
    });
    drawer.mouseleave(function(e) {
    	if(e.button==0){
    		drawer_draw = false ;
    	}
    	app.drawer_enable_scaling = true;
    });
})

export default func;